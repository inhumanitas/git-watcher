build:
	docker-compose build

up:
	docker-compose up -d

up-non-daemon:
	docker-compose up

up-multiple-workers:
	docker-compose up -d --scale celery_worker=12

start:
	docker-compose start

stop:
	docker-compose stop

restart:
	docker-compose stop && docker-compose start

shell-nginx:
	docker exec -ti g-w-nginx /bin/sh

shell-web:
	docker exec -ti g-w-web /bin/sh

shell-celery-worker1:
	docker exec -ti git-watcher_celery_worker_1 /bin/sh

shell-db:
	docker exec -ti g-w-db /bin/sh

shell-redis:
	docker exec -ti g-w-redis /bin/sh

log-nginx:
	docker-compose logs g-w-nginx

log-web:
	docker-compose logs g-w-web

log-db:
	docker-compose logs g-w-db

remove-containers:
	docker rm -f g-w-nginx g-w-web g-w-db g-w-redis g-w-celery-beat && docker ps -a

collectstatic:
	docker exec g-w-web /bin/sh -c "python manage.py collectstatic --noinput"

import_webhooks:
	docker exec g-w-web /bin/sh -c "python manage.py import_webhooks -w https://webhook.omprussia.ru/webhook/"

import_gitlab:
	docker exec g-w-web /bin/sh -c "python manage.py import_gitlab --all -s https://git.omprussia.ru/ -w /git_watcher/repositories/management/commands/urls.txt"

import_gitlab_all:
	docker exec g-w-web /bin/sh -c "python manage.py import_gitlab --all -s https://git.omprussia.ru/"

createadmin:
	docker exec g-w-web /bin/sh -c "echo \"from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.filter(is_superuser=True).delete(); User.objects.create_superuser('admin', 'admin@example.com', '1')\" | python manage.py shell"

update_interval:
	docker exec g-w-web /bin/sh -c "echo \"from repositories.models import Repository, Scheduler; import datetime;Scheduler.objects.all().update(next_sync_time=datetime.datetime.now())\" | python manage.py shell"

set_interval:
	docker exec g-w-web /bin/sh -c "echo \"from repositories.models import Repository, Scheduler; Scheduler.objects.all().update(interval=60); [r.save() for r in Repository.objects.all()] \" | python manage.py shell"

update_las_result:
	docker exec g-w-web /bin/sh -c "echo \"from repositories.models import Scheduler; [r.sync_last_result for r in Scheduler.objects.all()] \" | python manage.py shell"

report_by_urls:
	docker exec g-w-web /bin/sh -c "python manage.py report_by_urls -f /git_watcher/repositories/management/commands/urls.txt"

launch_by_urls:
	docker exec g-w-web /bin/sh -c "python manage.py launch_by_urls -f /git_watcher/repositories/management/commands/urls.txt"
