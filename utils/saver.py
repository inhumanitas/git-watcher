from operator import itemgetter

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'git_watcher.settings')

application = get_wsgi_application()

from django_celery_results.models import TaskResult
from repositories.models import Repository


def save(file_name, urls):
    urls.sort(key=itemgetter(1))
    with open(file_name, 'w') as fh:
        fh.writelines('\n'.join(
            [f'{u}\t{e}' for u, e in urls]
        ))
    return urls


file_path = '/git_watcher/repositories/management/commands/urls.txt'

with open(file_path) as fh:
    urls = fh.readlines()

success = []
error = []
unknown = []

for url in urls:
    import pdb;pdb.set_trace()
    try:
        repo = Repository.objects.get(fetch_url=url.strip())
    except Repository.DoesNotExist:
        unknown.append((url, 'No info'))
        continue

    try:
        last_result = TaskResult.objects.filter(
            task_kwargs=f"{{'repo_id': {repo.id}}}"
        ).latest(
            'date_done'
        )
    except TaskResult.DoesNotExist:
        unknown.append((url, 'Not executed'))
        continue

    if last_result.result == '[]':
        success.append((url, ))
    else:
        error.append((url, last_result.result))

print(success)
print(error)
save('success', success)
save('error', error)
save('unknown', unknown)
