from repositories.models import Repository

with open('repositories/management/commands/urls.txt') as fh:
    urls = fh.read()

for row in urls.split('\n'):
    if not row:
        continue
    url, dest = row.strip().split()
    Repository.objects.filter(fetch_url=url).delete()

    Repository(
        fetch_url=url,
        web_url=url,
        dest_url=dest,
    ).save()
