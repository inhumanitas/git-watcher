# Annotation


The service compares git repositories.
Verifies the relevance of commits and repository branches.

# Deploying

## Run deploy

To start deploy simply run command. 

> make up

Will be created containers:

- `g-w-db`: Database container with postgres 
- `g-w-redis`: Redis used as queue engine for celery
- `git-watcher_celery_beat_1`:   Celery beat that holds periodic tasks
- `git-watcher_celery_worker_1`: Celery worker that run tasks  
- `g-w-web`: Web server based on django framework and run by gunicorn on 8000 port
- `git-watcher_celery_flower_1`: Celery flower used to watch after queue and workers
- `g-w-nginx`: Proxying requests to web server

## Run deploy with many workers


Also available multiple workers deploy:

> make up-multiple-workers

Will create all previous containers and many `git-watcher_celery_worker` containers

## Stop all

To stop all containers run

> make stop
