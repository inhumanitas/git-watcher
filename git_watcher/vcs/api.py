import logging
import re

from git_watcher.settings import branches_exclude_re, tags_exclude_re
from repositories.models import Repository
from vcs.git_cmd import get_repo_branches, get_repo_tags
from vcs.gitlab_api import (
    get_gitlab_project, get_gitlab_branches, get_gitlab_tags,
    get_gitlab_client, get_group_project
)

logger = logging.getLogger(__name__)


def compare_refs(first, second):
    """Compare hash and ref name for equality and count
    :param first: list of tuples (hash, name)
    :param second: list of tuples (hash, name)
    :return: list of tuples that are not equal
    """
    invalid_ref = None

    if len(first) != len(second):
        return (set(first) - set(second)) | (set(second) - set(first))

    for datum in first:
        if datum not in second:
            invalid_ref = datum
            break

    return invalid_ref


def watch_git(repo_id):
    repo = Repository.objects.get(id=repo_id)

    upstream_heads = get_repo_branches(repo.fetch_url)
    upstream_tags = get_repo_tags(repo.fetch_url)

    group, project = get_group_project(repo.dest_url)

    logger.info(f'Processing {group}/{project}')
    proj = get_gitlab_project(get_gitlab_client(), group, project)
    if not proj:
        msg = f'Failed to get gitlab project {group}/{project}'
        logger.error(msg)
        raise ValueError(msg)

    dest_heads = filter(lambda x: not re.match(branches_exclude_re, x[1]),
                        get_gitlab_branches(proj))
    dest_tags = filter(lambda x: not re.match(tags_exclude_re, x[1]),
                       get_gitlab_tags(proj))

    result = []
    invalid_ref = compare_refs(upstream_heads, list(dest_heads))
    if invalid_ref:
        result.append(f'Branches are not equal! {invalid_ref}')

    invalid_ref = compare_refs(upstream_tags, list(dest_tags))
    if invalid_ref:
        result.append(f'Tags are not equal! {invalid_ref}')

    return result
