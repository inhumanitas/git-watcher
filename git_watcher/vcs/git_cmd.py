import os
import subprocess
import logging

from dateutil.parser import parse

logger = logging.getLogger(__name__)

GIT = 'git'
GIT_DIR = 'GIT_DIR'


class CommandError(Exception):
    pass


class GitError(CommandError):
    pass


class Command(object):
    process = None
    error_cls = CommandError

    def __init__(self, command, env):
        self.command = command
        try:
            self.process = subprocess.Popen(
                'LC_ALL=C ' + ' '.join(self.command),
                env=env,
                shell=True,
                stdin=None, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        except Exception as e:
            msg = '%s: %s' % (' '.join(command), e)
            logger.debug(msg)
            raise self.error_cls(msg)

    def __repr__(self):
        return ' '.join(self.command)

    def run(self):
        logger.debug('Executing: %s' % self)
        stdout, stderr = self.process.communicate()
        logger.debug('Result: %s' % self.process.returncode)

        if self.process.returncode:
            msg = "Command '%s' failed with %s" % (self, stderr)
            logger.debug(msg)
            raise self.error_cls(msg)

        return stdout


class GitCommand(Command):
    error_cls = GitError


class Git(object):

    def __init__(self, git_path, bare=False):
        super(Git, self).__init__()
        git = '.git'
        if bare:
            if git_path.rstrip('/').endswith(git):
                self.git_path = git_path
            else:
                self.git_path = os.path.join(git_path, git)
        else:
            self.git_path = os.path.join(git_path, git)

        env = os.environ.copy()
        env[GIT_DIR] = self.git_path.encode()
        self.env = env
        logger.debug("Uses git repository at: %s" % self.git_path)

    def __getattr__(self, name):
        name = name.replace('_', '-')

        def fun(*cmdv):
            command = [GIT, name]
            command.extend(cmdv)
            result = GitCommand(command, self.env).run()

            return result

        return fun

    def __repr__(self):
        return "<webhook_git_mirroring.vcs.git.Git> (%s)" % self.git_path


def remove_difference(refs):
    diff_sign = '^{}'
    uniq_refs = {}
    for ref_hash, ref_name in refs:
        clear_ref_name = ref_name.replace(diff_sign, '')
        if diff_sign in ref_name:
            uniq_refs[clear_ref_name] = ref_hash
        elif ref_name in uniq_refs:
            continue

        else:
            uniq_refs[ref_name] = ref_hash

    return [(h, r) for r, h in uniq_refs.items()]


def get_repo_tags(git_url):
    git = Git('/tmp/test')
    raw = git.ls_remote('--tags', git_url).decode("utf-8").strip()
    refs = [r.split() for r in raw.split('\n')]
    refs = [
        (h, name.replace('refs/tags/', '')) for h, name in filter(bool, refs)
    ]
    return remove_difference(refs)


def get_repo_branches(git_url):
    git = Git('/tmp/test')
    raw = git.ls_remote('--heads', git_url).decode("utf-8").strip()
    refs = [r.split() for r in raw.split('\n')]
    ref_suffix = 'refs/heads/'

    refs = [
        (h, name.replace(ref_suffix, '')) for h, name in filter(bool, refs)
        if ref_suffix in name and 'HEAD' not in name
    ]
    return remove_difference(refs)
