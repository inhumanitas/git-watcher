from django.test import TestCase

from vcs.api import compare_refs


class RefDiffTestCase(TestCase):

    def test_refs_equal(self):
        # empty
        self.assertFalse(compare_refs([], []))

        first = [
            ('35574a0c', 'ref1'),
            ('9a00ae0d', 'ref2'),
            ('9a004a0c', 'ref3'),
        ]
        second = [
            ('9a00ae0d', 'ref2'),
            ('35574a0c', 'ref1'),
            ('9a004a0c', 'ref3'),
        ]
        #
        self.assertFalse(compare_refs(first, second))
        self.assertFalse(compare_refs(second, first))
        #
        first = [('35574a0c', 'ref1')]
        second = [('35574a0c', 'ref1')]
        self.assertFalse(compare_refs(second, first))

    def test_refs_unequal(self):
        first = [('35574a0c', 'ref1')]
        second = [('4a0c3557', 'ref1')]
        self.assertTrue(compare_refs(first, second))
        self.assertTrue(compare_refs(second, first))
        #
        first = [('35574a0c', 'no_ref')]
        second = [('35574a0c', 'ref1')]
        self.assertTrue(compare_refs(first, second))
        self.assertTrue(compare_refs(second, first))
        #
        self.assertTrue(compare_refs([], [('1', 'ref')]))
        self.assertTrue(compare_refs([('1', 'ref')], []))
        #
        first = [
            ('35574a0c', 'ref1'),
            ('9a00ae0d', 'ref2'),
            ('9a004a0c', 'ref3'),
        ]
        second = [
            ('9a00ae0d', 'ref2'),
            ('35574a0c', 'ref1'),
        ]
        self.assertTrue(compare_refs(first, second))
        self.assertTrue(compare_refs(second, first))

    def test_compare_refs_not_changes_data(self):
        first = [('35574a0c', 'no_ref')]
        second = [('35574a0c', 'no_ref')]
        first_copy = first[:]
        second_copy = second[:]
        compare_refs(first, [])
        self.assertEqual(first, first_copy)
        self.assertEqual(second, second_copy)

    def test_more_values(self):
        first = [
            ('19ed1dba3e9be74f9b4d26f9c27703a8e239e511', '0.5.9'),
            ('3aea83f637801567b92021d6bd52d61e532435e9', '0.5.8'),
            ('6cf8bb7b3271c070a6fd5fe74103303c18403b50', '0.5.7'),
            ('7470e4c1e95fb613ee2c199ed55f745457c36001', '0.5.6'),
            ('463ac5f17a05b8314fb080a9651cb30de16c634d', '0.5.5'),
            ('e5edb7a70c4988c3add948619cbfbe3b709f2a4b', '0.5.4'),
            ('622f1abab4620c3f9a59c4a12b05294ddd4cdf6d', '0.5.3'),
            ('6e63daa6b46ea393592a3f00cfb0ab0a8409fc4d', '0.5.2'),
            ('adba5bae6187244fc8ef5efc0a26cb09b6463d0f', '0.5.1'),
            ('be89e340f977f8306e4b0b8dc3fbd7c74860035b', '0.5.0'),
            ('5fdc4a8bb84daaa0cf272122c95a8612dfc47cc6', '0.4.7'),
            ('561015b312086d3c81191684b7ad92e928d5e9a3', '0.4.6'),
            ('259d49d708a415f8f988f3ac7e11358bde6644b0', '0.4.5'),
            ('1f5921260eef438a0e67a062d5e24c61c71e17b4', '0.4.4'),
            ('7fe036cec66dbb530547a1eadbd293df9669d8c7', '0.4.3'),
            ('e847ff61b23c4eee72e517491e30187fd80f5c93', '0.4.2'),
            ('d0da1f5cd880a54c65d560913158570ad9066396', '0.4.1'),
            ('8e760886bd3687b2b110b25178b643d82048c0ae', '0.4.0'),
            ('258fad1383c9e23cca9539fe92090c1d1fc49a41', '0.3.0'),
            ('827c1ef0ba13d11da82523b9b40ae5ebadd2e2c4', '0.2.1')
        ]
        second = first[:]
        self.assertFalse(compare_refs(first, second))
