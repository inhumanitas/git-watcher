
import logging
from urllib.parse import urlparse

from gitlab import Gitlab, GitlabAuthenticationError, GitlabGetError

from git_watcher.settings import gitlab_url, gitlab_token, gitlab_api_version

logger = logging.getLogger(__name__)


class VCSError(Exception):
    pass


def get_group_project(url):
    url = url.strip()
    url = url[:-4] if url.endswith('.git') else url
    netloc = urlparse(url).netloc
    scheme, netloc, path = url.partition(netloc)
    group, *project = path.strip('/').split('/')
    return group, '/'.join(project)


def get_gitlab_client(url=None, token=None, version=None):
    if url is None:
        url = gitlab_url
    if token is None:
        token = gitlab_token
    if version is None:
        version = gitlab_api_version

    gl = Gitlab(url, token, api_version=version)
    try:
        gl.auth()
    except (GitlabAuthenticationError, GitlabGetError) as e:
        raise VCSError(e.error_message)
    return gl


def get_gitlab_project(client, group_name, project_name):
    try:
        found_project = client.projects.get('/'.join((group_name, project_name)))
    except GitlabGetError:
        projects = client.projects.list(search=project_name)
        found_project = None
        name_with_namespace = ' / '.join((group_name, project_name))
        for project in projects:
            if project.name_with_namespace == name_with_namespace:
                found_project = project
                break
        else:
            logger.error('Project not found: %s/%s' % (group_name, project_name))
    return found_project


def get_gitlab_branches(project):
    return [
        (b.commit['id'], b.name) for b in project.branches.list(all=True)
        if 'HEAD' not in b.name
    ]


def get_gitlab_tags(project):
    return [(t.commit['id'], t.name) for t in project.tags.list(all=True)]


def get_gitlab_projects(url=None, token=None, version=None, get_all=False):
    gl = get_gitlab_client(url, token, version)
    groups = gl.groups.list(all=get_all)
    for group in groups:
        for project in group.projects.list(all=get_all):
            yield project
