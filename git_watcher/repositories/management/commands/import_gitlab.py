import os
from operator import itemgetter

import logging

from django.core.management.base import BaseCommand

from repositories.models import Repository
from vcs.gitlab_api import get_gitlab_projects

logger = logging.getLogger(__name__)


def load_urls(filter_urls_path):
    result = []
    if filter_urls_path and os.path.exists(filter_urls_path):
        with open(filter_urls_path) as fh:
            result = fh.read().split()
    return result


def url_to_skip(url):
    excluded = ('danis', 'test_mirror')  # FIXME move to settings
    for exc in excluded:
        if exc in url:
            return True


class Command(BaseCommand):
    help = 'Import repositories from webhook server'

    def check_url(self, url):
        if Repository.objects.filter(fetch_url=url).exists():
            return False
        return True

    def handle(self, *args, **options):
        server, get_all = options['server'], options['all']
        filter_urls_path = options['white_list']
        whitelist = load_urls(filter_urls_path)
        if filter_urls_path and not whitelist:
            self.stdout.write(
                self.style.WARNING(f'Empty or inaccessible white list file'))
            return

        created = 0
        mirror_of = 'Mirror of '
        error_projects = []
        processed = 0
        for project in get_gitlab_projects(server, get_all=get_all):
            processed += 1
            projects_name = project.name
            self.stdout.write(self.style.SUCCESS(f'Processing {projects_name}'))

            description = project.description
            if not description:
                msg = f'Could not recognize project: {project.http_url_to_repo}'
                self.stdout.write(self.style.ERROR(msg))
                error_projects.append((projects_name, msg))
                continue

            if mirror_of in description:
                fetch_url = description.replace(mirror_of, '').strip()
            else:
                if not options.get('interactive', False):
                    msg = f'Could not recognize url: {projects_name}'
                    self.stdout.write(self.style.ERROR(msg))
                    error_projects.append((projects_name, msg))
                    continue

                self.stdout.write(
                    self.style.WARNING(f'Processing: {projects_name}\n{description}'))
                fetch_url = input('Enter the fetch url > ').strip()

            if not self.check_url(fetch_url):
                msg = f'{projects_name} already exists. Skipping..'
                self.stdout.write(self.style.WARNING(msg))
                error_projects.append((projects_name, msg))
                continue

            if (whitelist and
                    fetch_url not in whitelist and
                    project.http_url_to_repo not in whitelist):
                msg = f'{fetch_url} skipping by whitelist..'
                self.stdout.write(self.style.WARNING(msg))
                error_projects.append((projects_name, msg))
                continue

            if url_to_skip(project.http_url_to_repo):
                continue

            Repository.objects.create(
                web_url=fetch_url,
                fetch_url=fetch_url,
                dest_url=project.http_url_to_repo
            )
            created += 1

        error_projects.sort(key=itemgetter(1))
        self.stdout.write(
            self.style.SUCCESS('\n'.join([f'{u}: {e}' for u, e in error_projects])))

        self.stdout.write(
            self.style.SUCCESS(f'From {processed} urls imported {created}'))

    def add_arguments(self, parser):
        parser.add_argument(
            '-s',
            '--server',
            help='Gitlab server url'
        )
        parser.add_argument(
            '-w',
            '--white-list',
            help='Urls list to filter in import Gitlab'
        )
        parser.add_argument(
            '-i',
            '--interactive',
            action='store_true',
            help='Enter unrecognized fetch urls manually for each webhook url'
        )
        parser.add_argument(
            '-all',
            '--all',
            action='store_true',
            help='Fetch all urls from gitlab'
        )
