from operator import itemgetter

import logging

from django.core.management.base import BaseCommand
from django_celery_results.models import TaskResult

from repositories.models import Repository

logger = logging.getLogger(__name__)


def save(file_name, urls):
    urls.sort(key=itemgetter(1))
    with open(file_name, 'w') as fh:
        fh.writelines('\n'.join(
            [f'{u}\t{e}' for u, e in urls]
        ))
    return urls


class Command(BaseCommand):
    help = 'Report status for given urls list'

    def handle(self, *args, **options):
        file_path = options['file_path'] or 'urls.txt'
        with open(file_path) as fh:
            urls = fh.readlines()

        success = []
        error = []
        unknown = []

        for url in urls:
            try:
                repo = Repository.objects.get(fetch_url=url.strip())
            except Repository.DoesNotExist:
                unknown.append((url, 'No info'))
                continue

            try:
                last_result = TaskResult.objects.filter(
                    task_kwargs=f"{{'repo_id': {repo.id}}}"
                ).latest(
                    'date_done'
                )
            except TaskResult.DoesNotExist:
                unknown.append((url, 'Not executed'))
                continue

            if last_result.result == '[]':
                success.append((url, ''))
            else:
                error.append((url, last_result.result))

        self.stdout.write(self.style.SUCCESS(f'Validated {len(urls)} urls'))
        save('success', success)
        self.stdout.write(self.style.SUCCESS(f'Succeed {len(success)} urls'))
        save('error', error)
        self.stdout.write(self.style.SUCCESS(f'Errored {len(error)} urls'))
        save('unknown', unknown)
        self.stdout.write(self.style.SUCCESS(f'Unknown {len(unknown)} urls'))

    def add_arguments(self, parser):
        parser.add_argument(
            '-f',
            '--file-path',
            help='Path to files with urls to validate with'
        )
