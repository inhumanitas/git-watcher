
import logging

from django.core.management.base import BaseCommand

from repositories.models import Repository
from repositories.tasks import compare_repository

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Launch tasks for given urls list'

    def handle(self, *args, **options):
        urls = []
        if options['file_path']:
            file_path = options['file_path']
            with open(file_path) as fh:
                urls = fh.read()
        if options['url']:
            urls.append(options['url'])

        total = 0
        launched = 0
        for url in urls.split():
            total += 1
            try:
                repo = Repository.objects.get(fetch_url=url.strip())
            except Repository.DoesNotExist:
                self.stdout.write(
                    self.style.ERROR(f'Not found {url}'))
                continue
            compare_repository.delay(repo_id=repo.id)
            launched += 1

        self.stdout.write(self.style.SUCCESS(f'Got {total} urls'))
        self.stdout.write(self.style.SUCCESS(f'Launched {launched} urls'))

    def add_arguments(self, parser):
        parser.add_argument(
            '-f',
            '--file-path',
            help='Path to files with urls to validate with'
        )
        parser.add_argument(
            '-u',
            '--url',
            help='URL to run task'
        )
