from operator import itemgetter

import requests
import logging

from django.core.management.base import BaseCommand

from repositories.models import Repository
from vcs.gitlab_api import (
    get_gitlab_client, get_gitlab_project, get_group_project)

logger = logging.getLogger(__name__)


def get_webhook_url(webhook_server_url):
    api_url = '/api/webhookmappings/'
    url = webhook_server_url.rstrip('/') + api_url
    results = requests.get(url+'?limit=1')
    urls = []
    if results.status_code == 200:
        results = requests.get(url + f'?limit={results.json().get("count")}')
        for datum in results.json().get('results', {}):
            urls.append(datum.get('repourl', ''))
    return urls


class Command(BaseCommand):
    help = 'Import repositories from webhook server'

    def check_url(self, url):
        if Repository.objects.filter(fetch_url=url).exists():
            return False
        return True

    def handle(self, *args, **options):
        urls = get_webhook_url(options['webhook_server'])

        self.stdout.write(
            self.style.SUCCESS(f'Going to import {len(urls)} urls'))
        created = 0
        mirror_of = 'Mirror of '
        error_urls = []
        for url in urls:
            self.stdout.write(self.style.SUCCESS(f'Processing {url}'))

            group, proj = get_group_project(url)
            project = get_gitlab_project(get_gitlab_client(), group, proj)
            if not project:
                msg = f'Could not fetch project: {group}/{proj}'
                self.stdout.write(self.style.ERROR(msg))
                error_urls.append((url, msg))
                continue

            description = project.description
            if not description:
                msg = f'Could not recognize url: {url}'
                self.stdout.write(self.style.ERROR(msg))
                error_urls.append((url, msg))
                continue

            if mirror_of in description:
                fetch_url = description.replace(mirror_of, '').strip()
            else:
                if not options.get('interactive', False):
                    msg = f'Could not recognize url: {url}'
                    self.stdout.write(self.style.ERROR(msg))
                    error_urls.append((url, msg))
                    continue

                self.stdout.write(
                    self.style.WARNING(f'Processing: {url}\n{description}'))
                fetch_url = input('Enter the fetch url > ').strip()

            if not self.check_url(fetch_url):
                msg = f'{url} already exists. Skipping..'
                self.stdout.write(self.style.WARNING(msg))
                error_urls.append((url, msg))
                continue

            Repository.objects.create(
                web_url=url, fetch_url=fetch_url, dest_url=url)
            created += 1

        error_urls.sort(key=itemgetter(1))
        self.stdout.write(
            self.style.SUCCESS('\n'.join([f'{u}: {e}' for u, e in error_urls])))

        self.stdout.write(
            self.style.SUCCESS(f'From {len(urls)} urls imported {created}'))

    def add_arguments(self, parser):
        parser.add_argument(
            '-w',
            '--webhook-server',
            help='webhook server url'
        )
        parser.add_argument(
            '-i',
            '--interactive',
            action='store_true',
            help='Enter unrecognized fetch urls manually for each webhook url'
        )
