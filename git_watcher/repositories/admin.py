import csv
import logging

import pytz
from django.contrib import admin, messages
from django.http import HttpResponse
from django.urls import reverse
from django.utils.html import format_html

from repositories import models
from repositories.models import StatusEnum
from repositories.tasks import compare_repository


logger = logging.getLogger(__name__)

local_tz = pytz.timezone("Europe/Moscow")


@admin.register(models.Scheduler)
class RepositorySchedulerAdmin(admin.ModelAdmin):
    list_display = ['repository', 'last_sync_time', 'next_sync_time']


@admin.register(models.Repository)
class RepositoryAdmin(admin.ModelAdmin):
    list_display = [
        'web_url', 'interval', 'next_sync_time', 'scheduler__status',
        'last_result',
    ]

    search_fields = ['web_url', 'scheduler__last_result__result']
    exclude = ['state']

    actions = ['launch', 'export_urls']
    list_filter = ['scheduler__status']

    @staticmethod
    def interval(obj):
        scheduler = obj.scheduler
        url = reverse(
            'admin:repositories_scheduler_change', args=(scheduler.id,))
        return format_html(
            '<a href="{url}">{name}</a>'.format(
                url=url, name=scheduler.interval))

    @staticmethod
    def scheduler__status(obj):
        return StatusEnum.names.get(obj.scheduler.status, obj.scheduler.status)

    def next_sync_time(self, obj):
        return obj.scheduler.next_sync_time.astimezone(local_tz)
    next_sync_time.admin_order_field = 'scheduler__next_sync_time'

    def last_result(self, obj):
        task_result = obj.scheduler.last_result
        if task_result is None:
            return format_html('<div><p style="color:red;">-</p></div>')

        if task_result.result == '[]':
            return format_html('<div><p style="color:green;">Good</p></div>')
        else:
            msg = task_result.result.replace('{', '{{')
            msg = msg.replace('}', '}}')
            url = reverse(
                'admin:django_celery_results_taskresult_change',
                args=(task_result.id,))
            return format_html(f'<a href="{url}" style="color:red;">{msg}</a>')
    last_result.allow_tags = True
    last_result.admin_order_field = 'scheduler__last_result'

    def launch(self, request, queryset):
        for row in queryset:
            row.scheduler.start()
            compare_repository.delay(repo_id=row.id)
        messages.info(request, f'Launched {queryset.count()} repositories')
    launch.short_description = "Launch comparision"

    def export_urls(self, request, queryset):
        response = HttpResponse(content_type='text/csv')
        response[
            'Content-Disposition'] = 'attachment; filename=botactions_uid.csv'
        writer = csv.writer(response)
        urls = {obj.fetch_url for obj in queryset}
        writer.writerows([[uid] for uid in urls])
        return response

    export_urls.short_description = "Download urls"
