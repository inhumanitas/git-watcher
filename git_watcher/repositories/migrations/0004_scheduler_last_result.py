# Generated by Django 2.2.3 on 2019-07-17 20:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('django_celery_results', '0004_auto_20190516_0412'),
        ('repositories', '0003_auto_20190708_2006'),
    ]

    operations = [
        migrations.AddField(
            model_name='scheduler',
            name='last_result',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='django_celery_results.TaskResult'),
        ),
    ]
