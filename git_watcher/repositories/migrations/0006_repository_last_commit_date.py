# Generated by Django 2.2.3 on 2019-07-20 12:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('repositories', '0005_remove_repository_state'),
    ]

    operations = [
        migrations.AddField(
            model_name='repository',
            name='last_commit_date',
            field=models.DateTimeField(null=True),
        ),
    ]
