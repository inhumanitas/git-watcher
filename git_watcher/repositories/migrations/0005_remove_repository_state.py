# Generated by Django 2.2.3 on 2019-07-17 20:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('repositories', '0004_scheduler_last_result'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='repository',
            name='state',
        ),
    ]
