# Generated by Django 2.2.3 on 2019-07-07 21:34

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('repositories', '0001_initial'),
    ]

    operations = [
        migrations.DeleteModel(
            name='SchedulerHistory',
        ),
    ]
