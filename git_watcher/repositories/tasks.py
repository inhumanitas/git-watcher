import logging

from celery.app.control import Control
from django.utils import timezone

from git_watcher.celery import app
from git_watcher.settings import MAX_TASKS_TO_RUN
from repositories.models import Repository

from vcs.api import watch_git


logger = logging.getLogger(__name__)


@app.on_after_finalize.connect
def setup_periodic_tasks(sender, **kwargs):
    logger.info('Starting launcher')
    # Calls watchdog() every 1 minutes.
    sender.add_periodic_task(60, watchdog.s(), name='Watch for repositories')


@app.on_after_finalize.connect
def setup_expire_tasks(sender, **kwargs):
    logger.info('Starting task expire')
    # Calls watchdog() every 1 minutes.
    sender.add_periodic_task(60, expire_task.s(), name='Watch for statuses')


@app.task(bind=True, name='compare_repository')
def compare_repository(self, repo_id):
    logger.info(f'Request: {self.request} for {repo_id}')

    try:
        result = watch_git(repo_id)
    except Exception as e:
        logger.error(
            f'Request: {self.request.id} for <{repo_id}> errored with {e}')
        result = str(e)
    finally:
        repo = Repository.objects.get(id=repo_id)
        repo.scheduler.stop()
        repo.save()
    logger.info(
        f'Request: {self.request.id} for {repo_id} finished with {result}')
    return result


@app.task(name='expire_task', ignore_result=True)
def expire_task():
    from repositories.models import Scheduler

    logger.info('Expire task is awaken...')
    now = timezone.now()
    tasks_to_run = Scheduler.objects.filter(
        next_sync_time__lte=now,
    )
    for task in tasks_to_run:
        task.expire()


@app.task(name='watchdog', ignore_result=True)
def watchdog():
    from repositories.models import Scheduler,StatusEnum

    logger.info('Watchdog is awaken...')
    now = timezone.now()
    # get tasks to run
    tasks_to_run = Scheduler.objects.filter(
        status__in=(StatusEnum.empty, StatusEnum.expired)
    )

    logger.info(
        f'Need to sync {tasks_to_run.count()} tasks. '
        f'Limit for this run {MAX_TASKS_TO_RUN}'
    )

    launched = 0
    # execute tasks
    for task in tasks_to_run:
        if launched >= MAX_TASKS_TO_RUN:
            logger.info(f'Reached limit of tasks {MAX_TASKS_TO_RUN}')
            return

        repo_id = task.repository.id

        logger.info(f'Starting job for {task}')

        compare_repository.delay(repo_id=repo_id)
        launched += 1
