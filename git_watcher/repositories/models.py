from datetime import timedelta

from django.db import models
from django.utils import timezone
from django_celery_results.models import TaskResult


def append_dot_git(url):
    return url if url.endswith('.git') else url+'.git'


class StatusEnum:
    empty = '0'
    in_progress = '1'
    finished = '2'
    expired = '3'

    names = {
        empty: 'Not checked',
        in_progress: 'Running',
        finished: 'Finished',
        expired: 'Need to check',
    }


class Repository(models.Model):
    web_url = models.CharField(max_length=300)
    fetch_url = models.CharField(max_length=300, default='')
    dest_url = models.CharField(max_length=300)

    class Meta:
        verbose_name_plural = 'Repositories'

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        self.fetch_url = append_dot_git(self.fetch_url)
        self.web_url = append_dot_git(self.web_url)

        super().save(force_insert, force_update, using, update_fields)
        obj, _ = Scheduler.objects.get_or_create(repository=self)
        obj.save()

    def task_result(self):
        try:
            task_result = TaskResult.objects.filter(
                task_kwargs=f"{{'repo_id': {self.id}}}"
            ).latest(
                'date_done'
            )
        except TaskResult.DoesNotExist:
            task_result = None
        return task_result


class Scheduler(models.Model):
    repository = models.OneToOneField(Repository, on_delete=models.CASCADE)
    last_sync_time = models.DateTimeField(blank=True, null=True, default=None)
    next_sync_time = models.DateTimeField()
    status = models.CharField(max_length=15, default=StatusEnum.empty)
    last_result = models.ForeignKey(TaskResult, on_delete=models.SET_NULL,
                                    null=True, blank=True, default=None)
    interval = models.PositiveIntegerField(
        default=60*24, verbose_name='Interval (in minutes)')

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        if not self.last_sync_time:
            self.last_sync_time = timezone.now()

        self.next_sync_time = (
                self.last_sync_time + timedelta(minutes=self.interval))

        super().save(force_insert, force_update, using, update_fields)

    def sync_last_result(self):
        task_result = self.repository.task_result()
        self.last_result = task_result
        self.save()

    def start(self):
        self.last_result = None
        self.status = StatusEnum.in_progress
        self.save()

    def stop(self):
        self.last_sync_time = timezone.now()
        self.sync_last_result()
        self.status = StatusEnum.finished
        self.save()

    def expire(self):
        self.status = StatusEnum.expired
        self.save()
